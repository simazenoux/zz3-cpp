#include "point.hpp"

std::ostream& operator<<(std::ostream& out, const Point& point)
{
    point.afficher(out);
    return out;
}
