#include "cartesien.hpp"
#include "polaire.hpp"

Cartesien::Cartesien(Polaire const p)
{
    p.convertir(*this);
}

Cartesien::Cartesien(double const x, double const y): x(x), y(y)
{}

double Cartesien::getX() const
{
    return x;
}

double Cartesien::getY() const
{
    return y;
}

void Cartesien::setX(double const _x)
{
    x = _x;
}

void Cartesien::setY(double const _y)
{
    y = _y;
}

void Cartesien::afficher(std::ostream& flux) const
{
    flux << "(x=" << x << ";y=" << y << ")";
}

void Cartesien::convertir(Cartesien& c) const
{
    c.setX(x);
    c.setY(y);
}

void Cartesien::convertir(Polaire& p) const
{
    p.setAngle(x == 0 ? 0 : atan(y / x) * 180 / M_PI);
    p.setDistance(sqrt(pow(x, 2) + pow(y, 2)));
}
