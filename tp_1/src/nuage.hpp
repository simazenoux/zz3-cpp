#ifndef NUAGE_HPP
#define NUAGE_HPP

#include <vector>
#include "point.hpp"
#include "cartesien.hpp"
#include "polaire.hpp"


class Nuage
{
    private:
        using data_type = Point*;
        using container_type = std::vector<data_type>;
        container_type points;

    public:
        using const_iterator = container_type::const_iterator;

        void ajouter(Point&);
        const_iterator begin() const;
        const_iterator end() const;
        unsigned int size() const;
};

Cartesien barycentre(Nuage const);

class BarycentrePolaire
{
    public:
        Polaire operator()(Nuage const);
};

class BarycentreCartesien
{
    public:
        Cartesien operator()(Nuage const);
};


#endif