#include "nuage.hpp"

void Nuage::ajouter(Point& p)
{
    points.push_back(&p);
}

Nuage::const_iterator Nuage::begin() const
{
    return points.begin();
}

Nuage::const_iterator Nuage::end() const
{
    return points.end();
}

unsigned int Nuage::size() const
{
    return points.size();
}

Cartesien barycentre(Nuage const n)
{
    double x = 0;
    double y = 0;
    Cartesien c;
    for (Point* point: n)
    {
        point->convertir(c);
        x += c.getX();
        y += c.getY();
    }

    return Cartesien(x/n.size(), y/n.size());
}


Cartesien BarycentreCartesien::operator()(Nuage const n)
{
    return barycentre(n);
}

Polaire BarycentrePolaire::operator()(Nuage const n)
{
    return Polaire(barycentre(n));
}