#ifndef POLAIRE_HPP
#define POLAIRE_HPP

#include <iostream>
#include <sstream>
#include "point.hpp"
#include "cartesien.hpp"

class Polaire: public Point
{
    private:
        double angle;
        double distance;

    public:
        Polaire(double const angle = 0, double const distance = 0);
        Polaire(Cartesien const c);

        double getAngle() const;
        double getDistance() const;
        void setAngle(double const a);
        void setDistance(double const d);

        void afficher(std::ostream& flux) const override;
        void convertir(Cartesien& c) const override;
        void convertir(Polaire& p) const override;
};

#endif