#include "polaire.hpp"

Polaire::Polaire(double const angle, double const distance): angle(angle), distance(distance)
{}

Polaire::Polaire(Cartesien const c)
{
    c.convertir(*this);
}

double Polaire::getAngle() const
{
    return angle;
}

double Polaire::getDistance() const
{
    return distance;
}

void Polaire::setAngle(double const a)
{
    angle = a;
}

void Polaire::setDistance(double const d)
{
    distance = d;
}

void Polaire::afficher(std::ostream& flux) const
{
    flux << "(a=" << angle << ";d=" << distance << ")";
}

void Polaire::convertir(Cartesien& c) const
{
    c.setX(distance * (cos(angle * M_PI / 180)));
    c.setY(distance * (sin(angle * M_PI / 180)));
}

void Polaire::convertir(Polaire& p) const
{
    p.setAngle(getAngle());
    p.setDistance(getDistance());
}