#include <cstdlib>
#include <vector>
#include "point.hpp"
#include "cartesien.hpp"
#include "polaire.hpp"

int main() {
    Cartesien c1 = Cartesien{};
    Polaire p1 = Polaire{3,4};
    
    std::vector<Point*> v1;
    v1.push_back(&c1);
    v1.push_back(&p1);
    for (Point* point : v1)
    {
        std::cout << *point << std::endl;
    }

    std::vector<Point> v2;
    v2.push_back(c1);
    v2.push_back(p1);
    for (Point& point : v2)
    {
        std::cout << point << std::endl;
    }

    return EXIT_SUCCESS;
}
