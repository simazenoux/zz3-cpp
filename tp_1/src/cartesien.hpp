#ifndef CARTESIEN_HPP
#define CARTESIEN_HPP

#include "point.hpp"

class Cartesien: public Point
{
    private:
        double x;
        double y;

    public:
        Cartesien(double const x = 0, double const y = 0);
        Cartesien(Polaire const p);
        double getX() const;
        double getY() const;
        void setX(double const x);
        void setY(double const y);

        void afficher(std::ostream& flux) const;
        void convertir(Cartesien& c) const;
        void convertir(Polaire& p) const;
};
#endif