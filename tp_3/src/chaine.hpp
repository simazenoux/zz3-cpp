#ifndef CHAINE_HPP
#define CHAINE_HPP

#include <sstream>

#include "exception.hpp"

template <typename ... Args> std::string const chaine(Args... args)
{
    return ((chaine(args) + " ") + ...);
}

template <typename T> std::string const chaine(T x)
{
    throw ExceptionChaine(x);
}

template <> std::string const chaine(int x)
{
    return std::to_string(x);
}

template <> std::string const chaine(std::string const & x)
{
    return x;
}
    
template <> std::string const chaine(double x)
{
    return std::to_string(x);
}



#endif