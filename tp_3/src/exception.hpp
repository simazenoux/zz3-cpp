#ifndef EXCEPTION_HPP
#define EXCEPTION_HPP

#include <exception>
#include <string>

#include "demangle.hpp"

class ExceptionChaine: public std::exception
{
    private:
        std::string const __what;

    public:
        template <typename T> ExceptionChaine(T const x): __what("Conversion en chaine impossible pour '" +  demangle(typeid(x).name()) + "'")
        {}
        
        const char* what() const noexcept override
        {
            return __what.c_str();
        }
};

#endif