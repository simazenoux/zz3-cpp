#ifndef NUAGE_HPP
#define NUAGE_HPP

#include <vector>

template <typename T>
class Nuage
{
    private:
        using data_type = T;
        using container_type = std::vector<data_type>;
        container_type points;

    public:
        using const_iterator = typename container_type::const_iterator;

        void ajouter(T const & );
        const_iterator begin() const;
        const_iterator end() const;
        unsigned int size() const;
};

template <typename T>
void Nuage<T>::ajouter(T const & p)
{
    points.push_back(p);
}

template <typename T>
typename Nuage<T>::const_iterator Nuage<T>::begin() const
{
    return points.begin();
}

template <typename T>
typename Nuage<T>::const_iterator Nuage<T>::end() const
{
    return points.end();
}

template <typename T>
unsigned int Nuage<T>::size() const
{
    return points.size();
}

// Cartesien barycentre(Nuage const n)
// {
//     double x = 0;
//     double y = 0;
//     Cartesien c;
//     for (Point* point: n)
//     {
//         point->convertir(c);
//         x += c.getX();
//         y += c.getY();
//     }

//     return Cartesien(x/n.size(), y/n.size());
// }


// Cartesien BarycentreCartesien::operator()(Nuage const n)
// {
//     return barycentre(n);
// }

// Polaire BarycentrePolaire::operator()(Nuage const n)
// {
//     return Polaire(barycentre(n));
// }

// template <typename T>
// Cartesien barycentre(Nuage<T> const);

// class BarycentrePolaire
// {
//     public:
//         Polaire operator()(Nuage const);
// };

// class BarycentreCartesien
// {
//     public:
//         Cartesien operator()(Nuage const);
// };


#endif