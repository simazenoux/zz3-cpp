#include "cartesien.hpp"
#include "polaire.hpp"
#include "nuage.hpp"

Cartesien barycentre_v1(Nuage<Cartesien> const & n)
{
    if (n.size() == 0)
    {
        return Cartesien{};
    }

    double x = 0;
    double y = 0;
    for (Cartesien c: n)
    {
        x += c.getX();
        y += c.getY();
    }

    return Cartesien(x/n.size(), y/n.size());
}

template <typename T>
Cartesien barycentre_v1(Nuage<T> const & n)
{
    if (n.size() == 0)
    {
        return Cartesien{};
    }

    double x = 0;
    double y = 0;
    Cartesien c;
    for (T point: n)
    {
        point.convertir(c);
        x += c.getX();
        y += c.getY();
    }

    return Cartesien(x/n.size(), y/n.size());
}

template <typename T>
Cartesien barycentre_v2(T const & n)
{
    if (n.size() == 0)
    {
        return Cartesien{};
    }

    double x = 0;
    double y = 0;
    Cartesien c;
    for (T::data_type point: n)
    {
        point.convertir(c);
        x += c.getX();
        y += c.getY();
    }

    return Cartesien(x/n.size(), y/n.size());
}