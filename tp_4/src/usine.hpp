#ifndef FACTORY_HPP
#define FACTORY_HPP

#include <memory>
#include "carte.hpp"

class UsineCarte
{
    private:
        unsigned int valeur = 0;
        const unsigned int _size;

        UsineCarte(const UsineCarte&) = delete;
        UsineCarte& operator=(const UsineCarte&)  = delete;

    public:
        UsineCarte(const unsigned int size = 52);
        std::unique_ptr<Carte> getCarte();
};

#endif