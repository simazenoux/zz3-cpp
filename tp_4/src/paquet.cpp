#include "paquet.hpp"

void remplir(paquet_t & deck, UsineCarte & cardFactory)
{
    std::unique_ptr<Carte> p = cardFactory.getCarte();
    while (p)
    {
        deck.push_back(std::move(p));
        p = cardFactory.getCarte();
    }
}

std::ostream & operator<<(std::ostream & os, paquet_t const & deck)
{
    for (auto const & card: deck)
    {
        os << card->getValeur() << " ";
    }

    return os;
}
