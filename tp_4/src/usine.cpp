#include "usine.hpp"

UsineCarte::UsineCarte(const unsigned int size): _size(size)
{}

std::unique_ptr<Carte> UsineCarte::getCarte()
{
    if (valeur >= _size)
    {
        return nullptr;
    }
    std::unique_ptr<Carte> p(new Carte(valeur++));
    return p;
}