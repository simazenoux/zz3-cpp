#include "carte.hpp"

Carte::Carte(unsigned int valeur): _valeur(valeur)
{
    _counter++;
}

unsigned int Carte::getValeur()
{
    return _valeur;
}

unsigned int Carte::getCompteur()
{
    return _counter;
}

Carte::~Carte()
{
    _counter--;
}