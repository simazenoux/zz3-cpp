#ifndef DECK_HPP
#define DECK_HPP

#include <iostream>
#include <memory>
#include <vector>

#include "carte.hpp"
#include "usine.hpp"

using paquet_t = std::vector<std::unique_ptr<Carte>>;

void remplir(paquet_t &, UsineCarte &);

std::ostream & operator<<(std::ostream &, paquet_t const &);

#endif