#ifndef CARD_HPP
#define CARD_HPP

class Carte
{
    friend class UsineCarte;
    private:
        unsigned int _valeur;
        inline static unsigned int _counter = 0;

        Carte(unsigned int valeur);
        Carte(const Carte&) = delete;
        Carte & operator=(const Carte &) = delete;

    public:
        ~Carte();
        unsigned int getValeur();
        static unsigned int getCompteur();
};

#endif