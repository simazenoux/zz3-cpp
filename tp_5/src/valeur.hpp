#ifndef VALEUR_HPP
#define VALEUR_HPP

#include <string>

class Valeur
{
    private:
        double _score;
        std::string _name;

    public:
        Valeur(double const & score = 0.0, std::string const & name = "inconnu");
        double getNombre() const;
        void setNombre(double const & score);

        double getNote() const;
        void setNote(double const & score);

        std::string const & getEtudiant() const;
        void setEtudiant(std::string const & name);
};

#endif