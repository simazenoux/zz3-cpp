#ifndef COMPARATEUR_HPP
#define COMPARATEUR_HPP


template<class T>
class ComparateurQuantite
{
    public:
        bool operator()(T const & t1, T const & t2)
        {
            return (t1.getQuantite() == t2.getQuantite() && t1.getBorneInf() < t2.getBorneInf() || t1.getQuantite() > t2.getQuantite());
        }
};

#endif