#include "echantillon.hpp"

unsigned int Echantillon::getTaille() const
{
    return _valeurs.size();
}

void Echantillon::ajouter(double const v)
{
    _valeurs.push_back(v);
}

Valeur Echantillon::getMinimum() const
{
    if (_valeurs.size() == 0){
        throw std::domain_error("Empty list");
    }
    return *std::min_element(_valeurs.begin(), _valeurs.end(), [] (Valeur const & a, Valeur const & b) {return a.getNombre() < b.getNombre();});
}

Valeur Echantillon::getMaximum() const
{
    if (_valeurs.size() == 0){
        throw std::domain_error("Empty list");
    }
    return *std::max_element(_valeurs.begin(), _valeurs.end(), [] (Valeur const & a, Valeur const & b) {return a.getNombre() < b.getNombre();});
}

Valeur Echantillon::getValeur(unsigned int const i) const
{
    if (_valeurs.size() <= i){
        throw std::out_of_range("");
    }
    return _valeurs[i];
}