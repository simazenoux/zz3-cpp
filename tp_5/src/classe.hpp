#ifndef CLASSE_HPP
#define CLASSE_HPP

class Classe
{
    private:
        double _lower_bound;
        double _upper_bound;
        double _quantity;

    public:
        Classe(double const & lower_bound, double const & upper_bound);
        double getBorneInf() const;
        double getBorneSup() const;
        unsigned int getQuantite() const;
        void setBorneInf(double const);
        void setBorneSup(double const);
        void setQuantite(unsigned const n);
        void ajouter();
};

bool operator<(const Classe &a, const Classe &b);
bool operator>(Classe const &a, Classe const &b);

#endif