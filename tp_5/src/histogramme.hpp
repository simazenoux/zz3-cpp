#ifndef HISTOGRAMME_HPP
#define HISTOGRAMME_HPP

#include <algorithm>
#include <set>
#include "classe.hpp"
#include "echantillon.hpp"


template<class Compare = std::less<Classe>>
class Histogramme
{
    private:
        std::set<Classe, Compare> _classes;
        std::multimap

    public:
        Histogramme(double const & lower_bound, double const & upper_bound, unsigned const n)
        {
            double step = (upper_bound - lower_bound) / n;
            for (int i = lower_bound; i < upper_bound; i+= step)
            {
                _classes.insert(Classe(i, i+step));
            }
        }

        template<class T>
        Histogramme(Histogramme<T> const & h)
        {
            for (const Classe & c : h.getClasses()) {
                _classes.insert(c);
            }
        }

        std::set<Classe, Compare> const & getClasses() const
        {
            return _classes;
        }

        void ajouter(Echantillon const & e)
        {
            for (unsigned i = 0; i<e.getTaille(); i++)
            {
                ajouter(e.getValeur(i));
            }
        }

        void ajouter(Valeur const & e)
        {
            double const v = e.getNombre();
            auto const it = std::find_if(_classes.begin(), _classes.end(),
            [v] (Classe const & c) { return c.getBorneInf() <= v && v < c.getBorneSup(); });
            
            if (it != _classes.end())
            {
                Classe c = *it;
                _classes.erase(it);
                c.ajouter();
                _classes.emplace(c);
            }
        }
};

#endif