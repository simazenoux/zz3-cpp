#include "valeur.hpp"

Valeur::Valeur(double const & score, std::string const & name): _score(score), _name(name)
{}

double Valeur::getNombre() const
{
    return _score;
}

double Valeur::getNote() const
{
    return _score;
}

std::string const & Valeur::getEtudiant() const
{
    return _name;
}

void Valeur::setNombre(double const & a)
{
    _score = a;
}

void Valeur::setNote(double const & score)
{
    _score = score;
}

void Valeur::setEtudiant(std::string const & name)
{
    _name = name;
}
        