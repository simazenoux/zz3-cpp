#ifndef ECHANTILLON_HPP
#define ECHANTILLON_HPP

#include <algorithm>
#include <stdexcept>
#include <vector>
#include "valeur.hpp"

class Echantillon
{
    private:
        std::vector<Valeur> _valeurs;

    public:
        unsigned int getTaille() const;
        void ajouter(double const v);
        Valeur getMinimum() const;
        Valeur getMaximum() const;
        Valeur getValeur(unsigned int const i) const;
};

#endif