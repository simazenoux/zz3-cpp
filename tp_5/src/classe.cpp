#include "classe.hpp"

Classe::Classe(double const & lower_bound, double const & upper_bound): _lower_bound(lower_bound), _upper_bound(upper_bound), _quantity(0)
{}


double Classe::getBorneInf() const
{
    return _lower_bound;
}
double Classe::getBorneSup() const
{
    return _upper_bound;
}

unsigned int Classe::getQuantite() const
{
    return _quantity;
}

void Classe::setBorneInf(double const lower_bound)
{
    _lower_bound = lower_bound;
}

void Classe::setBorneSup(double const upper_bound)
{
    _upper_bound = upper_bound;
}

void Classe::setQuantite(unsigned const quantity)
{
    _quantity = quantity;
}

void Classe::ajouter()
{
    _quantity++;
}

bool operator<(const Classe &a, const Classe &b)
{
    return a.getBorneInf() < b.getBorneInf();
}

bool operator>(Classe const &a, Classe const &b)
{
    return a.getBorneInf() > b.getBorneInf();
}
